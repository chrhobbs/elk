# Running an ELK Stack (ElasticSearch, Logstash and Kibana) on Docker

Docker files for running ELK. This creates images for five services;
 - ElasticSearch to index the log data collected and make it more easily queryable
 - Logstash to act as a remote syslog to collect the logs from the containers
 - Logspout to actually send the container logs to Logstash
 - Kibana as a nice front end for interacting with the collected data
 - Cadvisor, a dashboard for monitoring container resource metrics

## Environment
Required

 - Docker
 - Docker Compose

## Downloading
Clone the repository

 
## Usage 
```
$ git clone 
$ cd elk
$ docker-compose -f docker-compose-quickstart.yml up 

```

This will boot up the application with prebaked images from Docker Hub, and your Kibana front-end will be accessible from port 80 of whatever host DOCKER_HOST points to.

Connect to port 80 on your host to connect to Kibana
Connect to port 8080 on host to connect to Cadvisor

Edit the docker-compose-common.yml file if you want to adjust the ports used for logging etc.


** Full credit to Nathan Claire

